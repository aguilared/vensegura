import React from 'react';
import {Header} from 'react-native-elements';
import {View, StyleSheet, Platform} from 'react-native';
import PropTypes from 'prop-types';

const navigation = this.props;
console.log('enappheader', navigation);
const AppHeader = ({headerText}) => (
  <View style={styles.container}>
    <Header
      leftComponent={{
        icon: 'menu',
        color: '#fff',
        onPress: () => navigation.openDrawer(),
      }}
      centerComponent={{text: headerText, style: {color: '#fff'}}}
      rightComponent={{
        icon: 'home',
        color: '#fff',
        onPress: () => navigation.openDrawer(),
      }}
      backgroundColor="#b20916"
    />
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 1,
    height: Platform.select({
      android: 56,
      default: 44,
    }),
  },
  headerContainer: {
    height: Platform.select({
      android: 56,
      default: 44,
    }),
  },
  title: {
    fontSize: 14,
    color: 'white',
    margin: 3,
  },
});

AppHeader.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default AppHeader;
