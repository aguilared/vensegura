import React from "react";
import { Header } from "react-native-elements";
import { View, StyleSheet } from 'react-native';


import HamburgerMenu from "./HamburgerMenu";

const MyHeader = props => {
  return (
    <View style={styles.container}>
      <Header
        leftComponent={<HamburgerMenu navigation={props.navigation} />}
        centerComponent={{
          text: props.title,
          style: { color: "#fff", fontWeight: "bold" }
        }}
        rightComponent={<HamburgerMenu navigation={props.navigation} />}
        statusBarProps={{ barStyle: "light-content" }}
        backgroundColor="#b20916"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 1,
    height: Platform.select({
      android: 56,
      default: 44,
    }),
  },
  headerContainer: {
    height: Platform.select({
      android: 56,
      default: 44,
    }),
  },
  title: {
    fontSize: 14,
    color: "white",
    margin: 3
  }
}); 

export default MyHeader;