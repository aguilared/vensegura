import React from 'react';
import { Header } from "react-native-elements";
import { View, StyleSheet } from "react-native";

const AppHeader = ({ headerText }) => (
  <View style={styles.container}>
    <Header
      centerComponent={{ text: headerText, style: { color: "#fff" } }}
      backgroundColor="#b20916"
    />
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 3
  },
  title: {
    fontSize: 14,
    color: "white",
    margin: 3
  }
}); 

export default AppHeader;