import React, {Component, Fragment} from 'react';
import {
  ActivityIndicator,
  View,
  Text,
  Image,
  FlatList,
  StatusBar,
} from 'react-native';
import {ListItem} from 'react-native-elements';
import {API} from '../constants';
import SearchBar1 from './SearchBar1';
import moment from 'moment';
import 'moment-timezone';
import styles from './styles';
import PropTypes from 'prop-types';

export default class SucesosSearch extends Component {
  state = {
    data: [],
    loading: true,
  };

  constructor(props) {
    super(props);
    this.onPressSearch = this.onPressSearch.bind(this);
  }
  static navigationOptions = {
    header: null,
    gesturesEnabled: false,
    tabBarVisible: false,
  };

  componentDidMount() {
    this.onPressSearch();
  }

  async onPressSearch(search) {
    let response = await fetch(
      `${API}v1/sucesos/findall8?search=${search ? search : ''}`,
    );
    response = await response.json();
    this.setState({loading: false, data: response});
  }

  keyExtractor = (item, index) => index.toString();
  renderItem = ({item}) => (
    <ListItem
      onPress={() =>
        this.props.navigation.navigate('Detalles', {
          fecha_suceso: item.fecha_suceso,
          id_suceso: item.suceso_id,
          titulo: item.titulo,
          detalle_delito: item.detalle_delito,
          municipio: item.municipio,
          parroquia: item.parroquia,
          nombre_victima: item.nombre_victima,
          edad: item.edad,
          sexo: item.sexo,
          sector: item.sector,
          mi_resena: item.mi_resena,
          fuente: item.fuente,
          uri: item.url_img,
          otherParam: 'Suceso Detallado',
        })
      }
      title={
        <View>
          <Image
            // eslint-disable-next-line react-native/no-inline-styles
            style={{
              alignSelf: 'center',
              height: 200,
              width: 275,
              borderWidth: 1,
              borderRadius: 10,
            }}
            source={{
              uri: item.url_img,
              cache: 'only-if-cached',
            }}
            resizeMode="stretch"
          />
        </View>
      }
      subtitle={
        <Text style={styles.sectionDescription}>
          {moment(item.fecha_suceso).format('DD/MM/YYYY HH:mm')} {item.titulo}
        </Text>
      }
    />
  );

  render() {
    if (this.state.loading) {
      return <ActivityIndicator size="large" color="#e91e63" />;
    }
    const date = new Date();
    const titulo = 'Ultimos Sucesos al: ' + moment(date).format('LLL');
    return (
      <Fragment>
        <StatusBar barStyle="dark-content" />
        <View style={styles.body}>
          <View style={styles.sectionContainer}>
            <SearchBar1 onPressSearch={this.onPressSearch} />
          </View>
          <View style={styles.sectionContainer}>
            <Text style={styles.sectionDescription}>{titulo}</Text>
          </View>
          <View style={styles.sectionContainer}>
            <FlatList
              keyExtractor={this.keyExtractor}
              data={this.state.data}
              renderItem={this.renderItem}
            />
          </View>
        </View>
      </Fragment>
    );
  }
}

SucesosSearch.propTypes = {
  navigation: PropTypes.object.isRequired,
};
