import React from 'react';
import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  text: {
    fontFamily: 'Avenir',
    fontSize: 16,
  },
  container: {
    flex: 1,
    margin: 3,
    backgroundColor: '#FF0312',
  },
  title: {
    fontSize: 14,
    backgroundColor: 'transparent',
    color: 'white',
  },
  icon: {
    width: 26,
    height: 26,
  },
  button: {
    alignSelf: 'stretch',
    marginLeft: 50,
    marginRight: 50,
    borderRadius: 5,
    height: 40,
    backgroundColor: '#A19E9E',
    justifyContent: 'center',
  },
  buttonText: {
    color: 'white',
    alignSelf: 'center',
    fontSize: 12,
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#34495e',
  },
  searchText: {
    flex: 1,
  },
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 2,
    marginBottom: 6,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionSubTitle: {
    fontSize: 16,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 16,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
  text1: {
    fontSize: 16,
    color: 'tomato',
    //fontFamily: 'Avenir',
  },
  subtitleView: {
    //flexDirection: 'row',
    paddingLeft: 10,
    marginBottom: 2,
    marginTop: 2,
  },
});

export default styles;
