import React, {Component} from 'react';
import {
  Linking,
  View,
  Text,
  Image,
  ScrollView,
  TouchableHighlight,
} from 'react-native';
import {Card, CardItem, Thumbnail, Body, Left} from 'native-base';
import moment from 'moment';
import 'moment-timezone';
import styles from './styles';
import PropTypes from 'prop-types';

export default class Detalles extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      title: navigation.getParam('otherParam', 'A Nested Details Screen'),
    };
  };
  render() {
    // The screen's current route is passed in to `props.navigation.state`:
    const {params} = this.props.navigation.state;
    const {navigation} = this.props;
    console.log('Navigation state', this.props.navigation.state);
    const otherParam = navigation.getParam('otherParam', 'some default value');
    console.log('Navigation itemId', otherParam);
    const fecha_suceso = params ? params.fecha_suceso : null;
    const titulo = params ? params.titulo : null;
    const fuente = params ? params.fuente : null;
    const nombre_victima = params ? params.nombre_victima : null;
    const edad = params ? params.edad : null;
    const sexo = params ? params.sexo : null;
    const municipio = params ? params.municipio : null;
    const parroquia = params ? params.parroquia : null;
    const mi_resena = params ? params.mi_resena : null;
    const uri = params ? params.uri : null;
    return (
      <View style={styles.container}>
        <ScrollView>
          <Card style={{borderRadius: 8}}>
            <CardItem
              header
              bordered
              style={{borderTopLeftRadius: 8, borderTopRightRadius: 8}}>
              <Left>
                <Thumbnail source={require('assets/me.png')} />
                <Body>
                  <Text>{`${moment(fecha_suceso).format('LLL')}`}</Text>
                  <Text note>{`${titulo}`}</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem
              button
              onPress={() => {
                Linking.openURL(`${fuente}`);
              }}>
              <Image
                source={{uri}}
                style={{
                  height: 200,
                  width: null,
                  flex: 1,
                  borderRadius: 10,
                  overflow: 'hidden',
                }}
                onPress={() => {
                  Linking.openURL(`${fuente}`);
                }}
              />
            </CardItem>
            <CardItem style={{height: 10}}>
              <Text>
                Municipio: {municipio}. Parroquia: {parroquia}
              </Text>
            </CardItem>
            <CardItem style={{height: 10}}>
              <Text>Victima: {nombre_victima} </Text>
            </CardItem>
            <CardItem style={{height: 10}}>
              <Text>
                Edad: {edad}. Sexo: {sexo}
              </Text>
            </CardItem>
            <CardItem>
              <Body>
                <Text style={{fontWeight: '200'}}>{mi_resena}</Text>
              </Body>
            </CardItem>
            <TouchableHighlight
              onPress={() => {
                Linking.openURL(`${fuente}`);
              }}
              style={[styles.button, {backgroundColor: 'red'}]}>
              <Text style={styles.buttonText}>Go To Source </Text>
            </TouchableHighlight>
          </Card>
        </ScrollView>
      </View>
    );
  }
}

Detalles.propTypes = {
  navigation: PropTypes.object.isRequired,
};
