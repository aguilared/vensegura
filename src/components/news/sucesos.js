import React, {Component, Fragment} from 'react';
import {
  ActivityIndicator,
  View,
  Text,
  FlatList,
  Image,
  StatusBar,
  Dimensions,
} from 'react-native';
import {API} from '../constants';
import {ListItem} from 'react-native-elements';
import styles from './styles';
import moment from 'moment';

require('moment-timezone');
let dimensions = Dimensions.get('window');
let imageHeight = Math.round((dimensions.width * 7) / 16);
let imageWidth = dimensions.width;
export default class Sucesos extends Component {
  state = {
    data: [],
    data2: [],
    data3: [],
    tmes: 0,
    totalAcuAno: 0,
    totalAcuDaysAno: 0,
    loading: true,
  };

  static navigationOptions = {
    header: null,
    gesturesEnabled: false,
    tabBarVisible: false,
  };

  componentDidMount() {
    this.fetchData();
  }

  fetchData = async () => {
    const response = await fetch(`${API}v1/sucesos/list3?page=0&per_page=20`);
    const response2 = await fetch(`${API}v1/sucesos/AcuAnoActual`);
    const response3 = await fetch(`${API}v1/sucesos/AcuDaysAnoBefore`);
    const json = await response.json();
    const json2 = await response2.json();
    const json3 = await response3.json();
    this.setState({loading: false, data: json});
    this.setState({data2: json2.acumu_meses});
    this.setState({data3: json3.acumu_days});

    this.setState({tmes: this.state.data2[0].tot_mes});
    this.setState({totalAcuAno: this.state.data2[0].totalAcuAno});
    this.setState({
      totalAcuDaysAno: this.state.data3[0].totalAcuDaysAno,
    });
    //console.log(this.state.data3[0].totalAcuDaysAno);
  };
  keyExtractor = (item, index) => index.toString();
  renderItem = ({item}) => (
    <ListItem
      onPress={() =>
        this.props.navigation.navigate('Detalles', {
          fecha_suceso: item.fecha_suceso,
          id_suceso: item.suceso_id,
          titulo: item.titulo,
          detalle_delito: item.detalle_delito,
          municipio: item.municipio,
          parroquia: item.parroquia,
          nombre_victima: item.nombre_victima,
          edad: item.edad,
          sexo: item.sexo,
          sector: item.sector,
          mi_resena: item.mi_resena,
          fuente: item.fuente,
          uri: item.url_img,
          otherParam: 'Suceso Detallado',
        })
      }
      title={
        <View
          // eslint-disable-next-line react-native/no-inline-styles
          style={{
            borderRadius: 10,
            overflow: 'hidden',
          }}>
          <Image
            // eslint-disable-next-line react-native/no-inline-styles
            style={{
              alignSelf: 'center',
              height: imageHeight,
              width: imageWidth,
              borderWidth: 1,
              borderRadius: 10,
            }}
            source={{
              uri: item.url_img,
              cache: 'only-if-cached',
            }}
            resizeMode="stretch"
          />
        </View>
      }
      subtitle={
        <Text style={styles.sectionDescription}>
          {moment(item.fecha_suceso).format('DD/MM/YYYY HH:mm')} {item.titulo}
        </Text>
      }
    />
  );

  render() {
    if (this.state.loading) {
      return <ActivityIndicator size="large" color="#e91e63" />;
    }
    var date = new Date();
    const titulo =
      'Ciudad Guayana, ultimos Sucesos al : ' +
      moment(date).format('DD/MM/YYYY') +
      ', Homicidios este mes: ' +
      this.state.tmes +
      ', acumulado en el año: ' +
      this.state.totalAcuAno +
      ', Homicidios a esta fecha año anterior: ' +
      this.state.totalAcuDaysAno;
    return (
      <Fragment>
        <StatusBar barStyle="dark-content" />
        <View style={styles.body}>
          <View style={styles.sectionContainer}>
            <Text style={styles.sectionDescription}>{titulo}</Text>
          </View>
          <View style={styles.sectionContainer}>
            <FlatList
              keyExtractor={this.keyExtractor}
              data={this.state.data}
              renderItem={this.renderItem}
            />
          </View>
        </View>
      </Fragment>
    );
  }
}
