import React from "react";
import { View, StyleSheet, Text } from "react-native";
import { MapView } from "expo";
import AppHeader from 'containers/AppHeader';
import moment from 'moment';
import 'moment-timezone';

export default class App extends React.Component {
  static navigationOptions = {
    headerTitle: <AppHeader
      headerText="Venezuela Segura - Bolivar, Caroni.    "
      style={{ height: 80 }}
    />,
    headerStyle: {
      backgroundColor: '#f4d2d2',
    },
  };
  
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      markers: [],
    };
  }

  fetchMarkerData() {
    fetch('http://18.216.139.132:1337/api/v1/sucesos/homianoparrocar')
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson);
        this.setState({
          isLoading: false,
          markers: responseJson.stationBeanList,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  componentDidMount() {
    this.fetchMarkerData();
  }

  render() {
    const date = new Date();
    const titulo = "Homicidios por Parroquias este año al: " + moment(date).format("LLL") + ", este mes: " ;
    return (
      <View style={styles.container}>     
        <Text style={styles.title}>{titulo}</Text> 
        <MapView
          style={{ flex: 1, height: 400, width: "100%" }}
          region={{
            latitude: 8.310839,
            longitude: -62.726316,
            latitudeDelta: 0.06922,
            longitudeDelta: 0.06421,
          }}
        >
          {this.state.isLoading ? null : this.state.markers.map((marker, index) => {
            const coords = {
              latitude: marker.latitude,
              longitude: marker.longitude,
            };

            const metadata = `+ : ${marker.total}`;

            return (
              <MapView.Marker
                key={index}
                coordinate={coords}
                title={marker.stationName}
                description={metadata}
              />
            );
          })}
        </MapView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 3,
    backgroundColor: '#FF0312',

  },
  title: {
    fontSize: 14,
    backgroundColor: 'transparent',
    color: 'white',
    margin: 3,
  },

}); 
