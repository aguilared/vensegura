import React from 'react';
import {
    StyleSheet
} from 'react-native';

const styles = StyleSheet.create({
  text: {
    fontFamily: 'Avenir',
  },
  container: {
    flex: 1,
    margin: 3,
    backgroundColor: '#FF0312',
  },
  title: {
    fontSize: 14,
    backgroundColor: 'transparent',
    color: 'white',
  },
  icon: {
    width: 26,
    height: 26,
  },
  button: {
    alignSelf: 'stretch',
    marginLeft: 50,
    marginRight: 50,
    borderRadius: 5,
    height: 40,
    backgroundColor: '#A19E9E',
    justifyContent: 'center'
  },
  buttonText: {
      color: 'white',
      alignSelf: 'center',
      fontSize: 12
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#34495e',
  },
  searchText:{
    flex: 1, 
  }
});

export default styles;