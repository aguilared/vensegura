import React, {Component, Fragment} from 'react';
import {
  ActivityIndicator,
  View,
  Text,
  ScrollView,
  FlatList,
} from 'react-native';
import {ListItem} from 'react-native-elements';
import AppHeader from './../../containers/AppHeader1';
import moment from 'moment';
import 'moment-timezone';
import styles from './../../components/news/styles';

export default class EstadisticasParr extends Component {
  state = {
    data: [],
    data2: [],
    data3: [],
    loading: true,
  };

  static navigationOptions = {
    headerTitle: <AppHeader headerText="Homicidios Ciudad Guayana" />,
  };

  componentDidMount() {
    this.fetchData();
  }

  fetchData = async () => {
    const response1 = await fetch(
      'http://18.216.139.132:1337/api/v1/sucesos/AcuAnoActual',
    ); //total del municipio
    const response2 = await fetch(
      'http://18.216.139.132:1337/api/v1/sucesos/AcuAnoActualParro1',
    ); //un solo objeto parroquia
    const response3 = await fetch(
      'http://18.216.139.132:1337/api/v1/sucesos/AcuAnoActualParro',
    ); //varios objetos x parroquias
    const json = await response1.json();
    const json2 = await response2.json();
    const json3 = await response3.json();
    this.setState({loading: false, data: json.acumu_meses});
    this.setState({data2: json2});
    this.setState({data3: json3.acu_mes});

    console.log(Object.keys(json3)); // ["first", "last"]
    //console.log('eale');
    //Object.keys(json3).forEach(key => console.log(key, json3[key]));
    var primerElemento = this.state.data3[0];
    //console.log('eale1');
    console.log('elemento del objeto:' + primerElemento);
  };

  _renderTitle(title) {
    return (
      // eslint-disable-next-line react-native/no-inline-styles
      <View style={{flex: 1, alignItems: 'center', marginTop: 20}}>
        <Text style={{fontSize: 20}}>{title}</Text>
      </View>
    );
  }

  render() {
    if (this.state.loading) {
      return <ActivityIndicator size="large" color="#e91e63" />;
    }
    const date = new Date();
    const titulo =
      'Homicidios Ciudad Guayana al: ' + moment(date).format('LLL');

    return (
      <Fragment>
        <View>
          <View style={styles.sectionContainer}>
            <Text style={styles.sectionDescription}>{titulo}</Text>
          </View>
          <ScrollView>
            <FlatList
              data={this.state.data}
              keyExtractor={(x, i) => i}
              renderItem={({item}) => (
                <ListItem
                  title={
                    <View style={styles.subtitleView}>
                      <Text style={styles.text}>
                        Total en el Año: {item.totalAcuAno}, este Mes:{' '}
                        {item.tot_mes}
                      </Text>
                      <Text style={styles.text}>
                        Enero: {item.ene}, Febrero: {item.feb}, Marzo:{' '}
                        {item.mar},{' '}
                      </Text>
                      <Text style={styles.text}>
                        Abril: {item.abr}, Mayo: {item.may}, Junio: {item.jun},{' '}
                      </Text>
                      <Text style={styles.text}>
                        Julio: {item.jul}, Agosto: {item.ago},{' '}
                      </Text>
                      <Text style={styles.text}>
                        Septiembre: {item.sep}, Octubre: {item.oct},{' '}
                      </Text>
                      <Text style={styles.text}>
                        Noviembre: {item.nov}, Diciembre: {item.dic},{' '}
                      </Text>
                    </View>
                  }
                />
              )}
            />
            <View style={styles.subtitleView}>
              <FlatList
                data={this.state.data2}
                keyExtractor={(x, i) => i}
                renderItem={({item}) => (
                  <ListItem
                    title={
                      <View>
                        <Text style={styles.text}>
                          Por Parroquias este AÑO: {item.total}{' '}
                        </Text>
                        <Text style={styles.text}>
                          Cachamay: {item.ano_parr_cacha}, Chirica:{' '}
                          {item.ano_parr_chi},{' '}
                        </Text>
                        <Text style={styles.text}>
                          Dalla Costa: {item.ano_parr_dalla}, Once Abril:{' '}
                          {item.ano_parr_once},{' '}
                        </Text>
                        <Text style={styles.text}>
                          Pozo Verde: {item.ano_parr_pozo}, Simon Bolvar:{' '}
                          {item.ano_parr_simon},{' '}
                        </Text>
                        <Text style={styles.text1}>
                          Unare: {item.ano_parr_unare}, Universidad:{' '}
                          {item.ano_parr_uni},{' '}
                        </Text>
                        <Text style={styles.text}>
                          Vistalsol: {item.ano_parr_vista}, Yocoima:{' '}
                          {item.ano_parr_yoco},{' '}
                        </Text>
                      </View>
                    }
                  />
                )}
              />
            </View>
            <View style={styles.subtitleView}>
              <FlatList
                data={this.state.data3}
                keyExtractor={(x, i) => i}
                renderItem={({item}) => (
                  <ListItem
                    title={
                      <View>
                        <Text style={styles.text}>
                          Por Parroquias este MES: {item.totalMes}{' '}
                        </Text>
                        <Text style={styles.text}>
                          Cachamay: {item.mes_parr_cacha}, Chirica:{' '}
                          {item.mes_parr_chi},{' '}
                        </Text>
                        <Text style={styles.text}>
                          Dalla Costa: {item.mes_parr_dalla}, Once Abril:{' '}
                          {item.mes_parr_once},{' '}
                        </Text>
                        <Text style={styles.text}>
                          Pozo Verde: {item.mes_parr_pozo}, Simon Bolvar:{' '}
                          {item.mes_parr_simon},{' '}
                        </Text>
                        <Text style={styles.text1}>
                          Unare: {item.mes_parr_unare}, Universidad:{' '}
                          {item.mes_parr_uni},{' '}
                        </Text>
                        <Text style={styles.text}>
                          Vistalsol: {item.mes_parr_vista}, Yocoima:{' '}
                          {item.mes_parr_yoco},{' '}
                        </Text>
                      </View>
                    }
                  />
                )}
              />
            </View>
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>Vensegura </Text>
              <Text style={styles.sectionDescription}>
                Bitacora de Sucesos de Ciudad Guayana y alrededores!
              </Text>
            </View>
          </ScrollView>
        </View>
        
      </Fragment>
    );
  }
}
