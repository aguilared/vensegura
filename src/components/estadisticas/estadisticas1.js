/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  ScrollView,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {API} from '../constants';
import {ListItem} from 'react-native-elements';
const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;
import moment from 'moment';
import 'moment-timezone';

export default class Estadisticas extends Component {
  enableBtn(index, item, arg2) {
    throw new Error("Method not implemented.");
  }
  constructor(props) {
    super(props);
    this.state = {
      data2: [],
      data4: [],
      data44: [],
      data444: [],
      data5: [],
      tmes: 0,
      totalAcuAno: 0,
      meses: ['Año', 'Tot', 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun'],
      meses2: ['Año', 'Tot', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    };
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData = async () => {
    const response2 = await fetch(`${API}v1/sucesos/AcuAnoActual`);
    const response4 = await fetch(`${API}v1/sucesos/acuanos6mesesp`); //Anos meses primer semestre
    const response44 = await fetch(`${API}v1/sucesos/acuanos6mesesu`); //Anos meses primer semestre
    const response5 = await fetch(`${API}v1/sucesos/acuanosmeses`); //Anos meses primer semestre

    const json2 = await response2.json();
    const json4 = await response4.json();

    const json44 = await response44.json();
    const json5 = await response5.json();
    //json4.sort();
    this.setState({data2: json2.acumu_meses});
    this.setState({data4: json4.homiAnoMesesCaro});
    //this.setState({ data444: json4.homiAnoMesesCaro });
    this.setState({data44: json44.homiAnoMesesCaro});
    this.setState({data5: json5.homiAnoMesesCaro});

    this.setState({tmes: this.state.data2[0].tot_mes});
    this.setState({totalAcuAno: this.state.data2[0].totalAcuAno});

    function compareValues(key, order = 'asc') {
      return function(a, b) {
        if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
          return 0;
        }

        const varA = typeof a[key] === 'string' ? a[key].toUpperCase() : a[key];
        const varB = typeof b[key] === 'string' ? b[key].toUpperCase() : b[key];

        let comparison = 0;
        if (varA > varB) {
          comparison = 1;
        } else if (varA < varB) {
          comparison = -1;
        }
        return order == 'desc' ? comparison * -1 : comparison;
      };
    }

    //console.log('sort:', this.state.data4.sort(compareValues('ano', 'desc')));
    this.setState.data4 = this.state.data4.sort(compareValues('ano', 'desc'));
    this.setState.data44 = this.state.data44.sort(compareValues('ano', 'desc'));
  };

  _renderTitle(title) {
    return (
      <View style={{flex: 1, alignItems: 'center', marginTop: 20}}>
        <Text style={{fontSize: 20}}>{title}</Text>
      </View>
    );
  }

  _keyExtractorMeses = (item, index) => index.toString();

  _renderItemMeses = ({item, index}) => (
    <View
      style={{
        width: WIDTH / 9,
        justifyContent: 'center',
        marginHorizontal: 2,
      }}
      key={index.toString()}>
      <Text
        style={[
          {
            textAlign: 'center',
            fontFamily: 'SourceSansPro-Regular',
            color: '#666666',
            fontSize: 14,
          },
          index == 0 && {color: '#f43f3f'},
        ]}>
        {item}
      </Text>
    </View>
  );
  _keyExtractorMeses2 = (item, index) => index.toString();

  _renderItemMeses2 = ({item, index}) => (
    <View
      style={{
        width: WIDTH / 9,
        justifyContent: 'center',
        marginHorizontal: 2,
      }}
      key={index.toString()}>
      <Text
        style={[
          {
            textAlign: 'center',
            fontFamily: 'SourceSansPro-Regular',
            color: '#666666',
            fontSize: 14,
          },
          index == 0 && {color: '#f43f3f'},
        ]}>
        {item}
      </Text>
    </View>
  );

  _keyExtractor = (item, index) => index.toString();

  _renderItem = ({item, index}) => {
    return (
      <View
        key={index.toString()}
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          marginBottom: 1,
        }}>
        <View
          style={{
            width: WIDTH / 9,
            justifyContent: 'center',
            alignItems: 'center',
            marginHorizontal: 2,
          }}>
          <TouchableOpacity
            onPress={() => this.enableBtn(index, item, 1)}
            style={[styles.btnDisabled, item['1'] && styles.btnEnable]}>
            <Text>{item.ano}</Text>
          </TouchableOpacity>
        </View>

        <View
          style={{
            width: WIDTH / 9,
            justifyContent: 'center',
            alignItems: 'center',
            marginHorizontal: 2,
          }}>
          <TouchableOpacity
            onPress={() => this.enableBtn(index, item, 2)}
            style={[styles.btnDisabled, item['2'] && styles.btnEnable]}>
            <Text style={{}}>{item.total}</Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: WIDTH / 9,
            justifyContent: 'center',
            alignItems: 'center',
            marginHorizontal: 2,
          }}>
          <TouchableOpacity
            onPress={() => this.enableBtn(index, item, 3)}
            style={[styles.btnDisabled, item['3'] && styles.btnEnable]}>
            <Text style={{}}>{item.ene}</Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: WIDTH / 9,
            justifyContent: 'center',
            alignItems: 'center',
            marginHorizontal: 2,
          }}>
          <TouchableOpacity
            onPress={() => this.enableBtn(index, item, 4)}
            style={[styles.btnDisabled, item['4'] && styles.btnEnable]}>
            <Text style={{}}>{item.feb}</Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: WIDTH / 9,
            justifyContent: 'center',
            alignItems: 'center',
            marginHorizontal: 2,
          }}>
          <TouchableOpacity
            onPress={() => this.enableBtn(index, item, 5)}
            style={[styles.btnDisabled, item['5'] && styles.btnEnable]}>
            <Text style={{}}>{item.mar}</Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: WIDTH / 9,
            justifyContent: 'center',
            alignItems: 'center',
            marginHorizontal: 2,
          }}>
          <TouchableOpacity
            onPress={() => this.enableBtn(index, item, 6)}
            style={[styles.btnDisabled, item['6'] && styles.btnEnable]}>
            <Text style={{}}>{item.abr}</Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: WIDTH / 9,
            justifyContent: 'center',
            alignItems: 'center',
            marginHorizontal: 2,
          }}>
          <TouchableOpacity
            onPress={() => this.enableBtn(index, item, 7)}
            style={[styles.btnDisabled, item['7'] && styles.btnEnable]}>
            <Text style={{}}>{item.may}</Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: WIDTH / 9,
            justifyContent: 'center',
            alignItems: 'center',
            marginHorizontal: 2,
          }}>
          <TouchableOpacity
            onPress={() => this.enableBtn(index, item, 8)}
            style={[styles.btnDisabled, item['8'] && styles.btnEnable]}>
            <Text style={{}}>{item.jun}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  _keyExtractorm2 = (item, index) => index.toString();

  _renderItemm2 = ({item, index}) => {
    return (
      <View
        key={index.toString()}
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'stretch',
          marginBottom: 1,
        }}>
        <View
          style={{
            width: WIDTH / 9,
            justifyContent: 'center',
            alignItems: 'center',
            marginHorizontal: 2,
          }}>
          <TouchableOpacity
            onPress={() => this.enableBtn(index, item, 1)}
            style={[styles.btnDisabled, item['1'] && styles.btnEnable]}>
            <Text>{item.ano}</Text>
          </TouchableOpacity>
        </View>

        <View
          style={{
            width: WIDTH / 9,
            justifyContent: 'center',
            alignItems: 'center',
            marginHorizontal: 2,
          }}>
          <TouchableOpacity
            onPress={() => this.enableBtn(index, item, 2)}
            style={[styles.btnDisabled, item['2'] && styles.btnEnable]}>
            <Text style={{}}>{item.total}</Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: WIDTH / 9,
            justifyContent: 'center',
            alignItems: 'center',
            marginHorizontal: 2,
          }}>
          <TouchableOpacity
            onPress={() => this.enableBtn(index, item, 3)}
            style={[styles.btnDisabled, item['3'] && styles.btnEnable]}>
            <Text style={{}}>{item.jul}</Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: WIDTH / 9,
            justifyContent: 'center',
            alignItems: 'center',
            marginHorizontal: 2,
          }}>
          <TouchableOpacity
            onPress={() => this.enableBtn(index, item, 4)}
            style={[styles.btnDisabled, item['4'] && styles.btnEnable]}>
            <Text style={{}}>{item.ago}</Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: WIDTH / 9,
            justifyContent: 'center',
            alignItems: 'center',
            marginHorizontal: 2,
          }}>
          <TouchableOpacity
            onPress={() => this.enableBtn(index, item, 5)}
            style={[styles.btnDisabled, item['5'] && styles.btnEnable]}>
            <Text style={{}}>{item.sep}</Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: WIDTH / 9,
            justifyContent: 'center',
            alignItems: 'center',
            marginHorizontal: 2,
          }}>
          <TouchableOpacity
            onPress={() => this.enableBtn(index, item, 6)}
            style={[styles.btnDisabled, item['6'] && styles.btnEnable]}>
            <Text style={{}}>{item.oct}</Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: WIDTH / 9,
            justifyContent: 'center',
            alignItems: 'center',
            marginHorizontal: 2,
          }}>
          <TouchableOpacity
            onPress={() => this.enableBtn(index, item, 7)}
            style={[styles.btnDisabled, item['7'] && styles.btnEnable]}>
            <Text style={{}}>{item.nov}</Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: WIDTH / 9,
            justifyContent: 'center',
            alignItems: 'center',
            marginHorizontal: 2,
          }}>
          <TouchableOpacity
            onPress={() => this.enableBtn(index, item, 8)}
            style={[styles.btnDisabled, item['8'] && styles.btnEnable]}>
            <Text style={{}}>{item.dic}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  render() {
    const date = new Date();
    //const titulo = "Homicidios Ciudad Guayana al: " + moment(date).format("LLL");
    const titulo =
      'Homicidios Ciudad Guayana al: ' +
      moment(date).format('DD/MM/YYYY') +
      ', Homicidios este mes: ' +
      this.state.tmes +
      ', año= ' +
      this.state.totalAcuAno;
    const {navigate} = this.props.navigation;
    return (
      <View style={styles.container}>
        <ScrollView>
          <Text style={styles.title}>{titulo}</Text>
          <View style={styles.subtitleView}>
            <Text style={styles.text}>
              Historico de Homicidios por años. Primer Semestre
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'stretch',
              marginBottom: 1,
            }}>
            <FlatList
              numColumns={8}
              contentContainerStyle={{}}
              data={this.state.meses}
              extraData={this.state}
              keyExtractor={this._keyExtractorMeses}
              renderItem={this._renderItemMeses}
            />
          </View>
          <View>
            <FlatList
              contentContainerStyle={{}}
              data={this.state.data4}
              extraData={this.state}
              keyExtractor={this._keyExtractor}
              renderItem={this._renderItem}
            />
          </View>
          <View style={styles.subtitleView}>
            <Text style={styles.text}>Segundo Semestre</Text>
          </View>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'stretch',
              marginBottom: 1,
            }}>
            <FlatList
              numColumns={8}
              contentContainerStyle={{}}
              data={this.state.meses2}
              extraData={this.state}
              keyExtractor={this._keyExtractorMeses2}
              renderItem={this._renderItemMeses2}
            />
          </View>

          <View>
            <FlatList
              contentContainerStyle={{}}
              data={this.state.data44}
              extraData={this.state}
              keyExtractor={this._keyExtractorm2}
              renderItem={this._renderItemm2}
            />
          </View>
          <View>
            <FlatList
              data={this.state.data5}
              keyExtractor={(x, i) => i}
              renderItem={({item}) => (
                <ListItem
                  roundAvatar
                  onPress={
                    () => navigate('news', {}) //x = item ..
                  }
                  subtitle={
                    <View style={styles.subtitleView}>
                      <Text style={styles.text}>
                        Año: {item.ano} Total: {item.total},{' '}
                      </Text>
                      <Text style={styles.text}>
                        Enero: {item.ene}, Febrero: {item.feb}, Marzo:{' '}
                        {item.mar},{' '}
                      </Text>
                      <Text style={styles.text}>
                        Abril: {item.abr}, Mayo: {item.may}, Junio: {item.jun},{' '}
                      </Text>
                      <Text style={styles.text}>
                        Julio: {item.jul}, Agosto: {item.ago}, Septiembre:{' '}
                        {item.sep},{' '}
                      </Text>
                      <Text style={styles.text}>
                        Octubre: {item.oct}, Noviembre: {item.nov}, Diciembre:{' '}
                        {item.dic},{' '}
                      </Text>
                    </View>
                  }
                />
              )}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 4,
    backgroundColor: '#ffffff',
  },
  title: {
    fontSize: 14,
    backgroundColor: 'transparent',
    color: '#170102',
    margin: 10,
  },
  subtitleView: {
    //flexDirection: 'row',
    paddingLeft: 10,
    paddingBottom: 5,
    paddingTop: 5,
  },
  text: {
    fontSize: 14,
    paddingTop: 5,
    //fontFamily: 'Avenir',
  },
  text1: {
    fontSize: 16,
    color: 'tomato',
    //fontFamily: 'Avenir',
  },
  btnDisabled: {
    borderWidth: 1,
    borderColor: '#eeeeee',
    backgroundColor: 'white',
    height: HEIGHT / 26,
    borderRadius: HEIGHT / 26 / 2,
  },
  btnEnable: {
    borderColor: '#00d1d1',
    backgroundColor: '#00d1d1',
  },
  btnPrimary: {
    backgroundColor: '#ff8800',
    borderColor: '#ff8800',
    borderWidth: 2,
    borderRadius: 20,
  },
  btnTxt: {
    fontFamily: 'SourceSansPro-Bold',
    fontSize: 14,
    paddingVertical: 7,
    paddingHorizontal: 14,
    textAlign: 'center',
    color: '#FFF',
  },
});
