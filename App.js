/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {View, Image, TouchableOpacity, Button, Dimensions} from 'react-native';
import {createAppContainer} from 'react-navigation';
import {createDrawerNavigator} from 'react-navigation-drawer';
import {createStackNavigator} from 'react-navigation-stack';
import Sucesos from './src/components/news/sucesos';
import Detalles from './src/components/news/detalles2';
import Estadisticas from './src/components/estadisticas/estadisticas1';
import EstadisticasParr from './src/components/estadisticas/estadisticasParr';
import SucesosSearch from './src/components/news/sucesosSearch';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import CustomSidebarMenu from './CustomSidebarMenu';

class NavigationDrawerStructure extends Component {
  //Structure for the navigatin Drawer
  toggleDrawer = () => {
    //Props to open/close the drawer
    this.props.navigationProps.toggleDrawer();
  };
  render() {
    return (
      <View style={{flexDirection: 'row'}}>
        <TouchableOpacity onPress={this.toggleDrawer.bind(this)}>
          {/*Donute Button Image */}
          <Image
            source={require('./image/drawer.png')}
            style={{width: 25, height: 25, marginLeft: 5}}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

const DrawerNavigatorExample = createDrawerNavigator({
  //Drawer Optons and indexing
    Home: {
      screen: Sucesos,
      navigationOptions: {
        drawerLabel: 'Home',
      },
    },

    Search: {
      screen: SucesosSearch,
      navigationOptions: {
        drawerLabel: 'Search',
      },
    },
    Estadisticas: {
      screen: Estadisticas,
      navigationOptions: {
        drawerLabel: 'Estadisticas',
      },
    },
    EstadisticasParr: {
      screen: EstadisticasParr,
      navigationOptions: {
        drawerLabel: 'EstadisticasParr',
      },
    },
  },
  {
    //For the Custom sidebar menu we have to provide our CustomSidebarMenu
    contentComponent: CustomSidebarMenu,
    //Sidebar width
    drawerWidth: Dimensions.get('window').width - 130,
  }
);

const stackNavigator = createStackNavigator(
  {
    Home: { screen: Sucesos },
    Detalles: { screen: Detalles },
    SucesosSearch: { screen: SucesosSearch },
    Estadisticas: { screen: Estadisticas },
    EstadisticasParr: { screen: EstadisticasParr },
    Drawer: {
      screen: DrawerNavigatorExample,
      navigationOptions: ({ navigation }) => ({
        title: 'Venezuela Segura',
        headerLeft: <NavigationDrawerStructure navigationProps={navigation} />,
        headerStyle: {
          backgroundColor: '#b20916',
          fontSize: 14,
        },
        headerTintColor: '#fff',
        headerRight: (
          <TouchableOpacity onPress={() => navigation.navigate('Search')}>
            {/*Donute Button Image */}
            <Image
              source={require('./image/search.png')}
              style={{ width: 25, height: 25, marginRight: 5 }}
            />
          </TouchableOpacity>
        ),
      }),
    },
  },
  {
    initialRouteName: 'Drawer',
  }
);

const App = createBottomTabNavigator(
  {
    Home: { screen: stackNavigator,
      path: '/home',
      navigationOptions: {
        tabBarIcon: ({ focused, tintColor }) => {
          const iconName = 'home';
          const tintColor111 = '#e91e63';
          return <MaterialIcons name={iconName} size={25} color={tintColor111} />;
        },
      },
    },
    Search: {
      screen: SucesosSearch,
      path: '/search',
      navigationOptions: {
        tabBarIcon: ({ focused, tintColor }) => {
          const iconName = 'search';
          return <MaterialIcons name={iconName} size={25} color={tintColor} />;
        },
      },
    },
    Estadisticas: {
      screen: Estadisticas,
      path: '/estadisticas',
      navigationOptions: {
        tabBarIcon: ({ focused, tintColor }) => {
          const iconName = 'assessment';
          return <MaterialIcons name={iconName} size={25} color={tintColor} />;
        },
      },
    },
  },
  {
    tabBarOptions: {
      activeTintColor: '#e91e63',
      tabStyle: {
        width: 100,
        backgroundColor: 'White',
      },
      labelStyle: {
        color: '#ACB6D4',
      },
    },
    initialRouteName: 'Home',

  }
  
);

export default createAppContainer(App);
